extends Node2D

class_name HighScoreManager

const SAVE_FILE_PATH = "user://highscores.save"
static var score = 0
var highscore : Dictionary

func _ready():
	# score = randi() % 1000
	load_highscore()
	display_player_score()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func display_player_score():
	var score_node = $Panel_GameOver/RTL_Score
	print("Display player score (%d) in %s" % [score,score_node] )
	if score_node != null:
		score_node.text = "%d"%score
	if check_highscore():
		$Panel_GameOver/LE_PlayerName.visible = true 
		$Panel_GameOver/RTL_LabelPlayerName.visible = true 
	else:  
		$Panel_GameOver/LE_PlayerName.visible = false
		$Panel_GameOver/RTL_LabelPlayerName.visible = false 
	
func save_highscore(name : String, score : int):
	var save_data = FileAccess.open(SAVE_FILE_PATH,FileAccess.READ)
	var highscore : Dictionary
	if save_data != null:
		highscore = JSON.parse_string( save_data.get_line() )
		highscore[name] = score
		save_data.close()
	else:
		highscore = { name : score }
		
	if highscore.size() > 5:
		# sort highscores
		var players = highscore.keys()
		players.sort_custom( 
			func(a,b) :
				return highscore[a] > highscore[b]
		)
		# remove last one
		highscore.erase(players[-1])
		
	save_data = FileAccess.open(SAVE_FILE_PATH, FileAccess.WRITE)
	save_data.store_line(JSON.stringify(highscore))

func check_highscore():
	if highscore.keys().size() < 5:
		return true 
	for key in highscore:
		if score > highscore[key]:
			return true
	return false 

func load_highscore():
	var save_data = FileAccess.open(SAVE_FILE_PATH,FileAccess.READ)
	if save_data != null:
		highscore = JSON.parse_string( save_data.get_line() )
		save_data.close()
	
	# sort highscores
	var players = highscore.keys()
	players.sort_custom( 
		func(a,b) :
			return highscore[a] > highscore[b]
	)
	
	print("Loaded %d highscores " % highscore.keys().size())
	
	# load them
	var player_names : String = ""
	var scores : String = ""
	var i : int = 0
	for key in players:
		player_names += "[b]%s[/b]\n" % key
		scores += "%s\n" % highscore[key]
		i += 1
		if i >= 5: 
			break
		
	$Panel_HighScores/RTL_Names.text = player_names
	$Panel_HighScores/RTL_Scores.text = scores

func _on_button_submit_pressed():
	load_highscore()
	if check_highscore():
		save_highscore( $Panel_GameOver/LE_PlayerName.text, score )
	load_highscore()

func _on_line_edit_gui_input(event):
	if event is InputEventKey:
		if event.pressed and event.keycode == KEY_ENTER:
			_on_button_submit_pressed()
