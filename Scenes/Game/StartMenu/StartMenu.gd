extends Node2D

class_name StartMenu

enum StartPanel {
	MAINMENU,
	GAMEOVER
}

static var starting_view : StartPanel = StartPanel.MAINMENU

func _ready():
	if starting_view == StartPanel.GAMEOVER:
		gameover_view()
	else:
		mainmenu_view()	
	if OS.has_feature("web"):
		$Canvas/Panel_Buttons/Button_Quit.visible = false

func _process(delta):
	pass

func mainmenu_view():
	$Canvas/Panel_Buttons.visible = true
	$Canvas/Panel_Credits.visible = false
	$Canvas/HighScoreManager/Panel_GameOver.visible = false
	$Canvas/HighScoreManager/Panel_HighScores.visible = false
	
func gameover_view():
	$Canvas/Panel_Buttons.visible = false
	$Canvas/Panel_Credits.visible = false
	$Canvas/HighScoreManager/Panel_GameOver.visible = true
	$Canvas/HighScoreManager/Panel_HighScores.visible = false
	$Canvas/HighScoreManager/Panel_GameOver/LE_PlayerName.grab_focus()
	
func highscores_view():
	$Canvas/Panel_Buttons.visible = false
	$Canvas/Panel_Credits.visible = false
	$Canvas/HighScoreManager/Panel_GameOver.visible = false
	$Canvas/HighScoreManager/Panel_HighScores.visible = true

func _on_button_start_pressed():
	#GameManager._reset_product_cart_count()
	get_tree().change_scene_to_file("res://Scenes/Game/Gameplay/Gameplay.tscn")

func _on_button_credits_pressed():
	$Canvas/Panel_Buttons.visible = false
	$Canvas/Panel_Credits.visible = true
	$Canvas/HighScoreManager/Panel_GameOver.visible = false
	$Canvas/HighScoreManager/Panel_HighScores.visible = false

func _on_button_quit_pressed():
	get_tree().quit()

func _on_button_back_pressed():
	mainmenu_view()

func _on_button_submit_pressed():
	highscores_view()

func _on_le_player_name_gui_input(event):
	if event is InputEventKey:
		if event.pressed and event.keycode == KEY_ENTER:
			_on_button_submit_pressed()

