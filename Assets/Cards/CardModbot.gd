extends Card

@export var ban_likes : bool
@export var chance : float 

var _controversy_hashtag : String
var _controversy_hashtag_symbol 

func _ready():
	_controversy_hashtag = User.hashtags.keys().pick_random()
	_controversy_hashtag_symbol = User.hashtags[_controversy_hashtag]
	super._ready()

func GetTitle():
	return "bAIsed MODBOT+ " + _controversy_hashtag_symbol

func GetShortDescription():
	return "🥾" + _controversy_hashtag_symbol + (" 🟢" if ban_likes else " 🔴" )

func GetLongDescription():
	return ("Randomly kicks users with " + _controversy_hashtag_symbol  + 
		(" like" if ban_likes else " hate"))
		
func onDraw():
	_controversy_hashtag = User.hashtags.keys().pick_random()
	_controversy_hashtag_symbol = User.hashtags[_controversy_hashtag]
	
	$CardTitle.text = GetTitle()
	UseShortDescription()

func PlayCard():
	print("Playing " + GetTitle())
	super.PlayCard()
	if ban_likes:
		CE_kickUsersWithDislike.emit(_controversy_hashtag,null,chance)
	else:
		CE_kickUsersWithDislike.emit(null,_controversy_hashtag,chance)
