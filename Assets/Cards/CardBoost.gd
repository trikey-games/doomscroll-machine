extends Card

@export var boost_factor : float = 1.5

func PlayCard():
	super.PlayCard()
	CE_userPrepareBoostFactor.emit(boost_factor)
