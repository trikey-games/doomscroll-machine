extends Card

@export var ban_chance : float
@export var money_amount : int

func PlayCard():
	print("Playcard in CardAd")
	super.PlayCard()
	CE_kickUsersWithChance.emit(ban_chance)
	CE_changeMoneyByAmount.emit(money_amount)
