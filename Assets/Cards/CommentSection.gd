extends Card

func UseLongDescription():
	$CardExtraLongDescription.text = GetLongDescription()
	$CardShortDescription.visible = false
	$CardExtraLongDescription.visible = true

func UseShortDescription():
	$CardShortDescription.text = GetShortDescription()
	$CardExtraLongDescription.visible = false
	$CardShortDescription.visible = true
