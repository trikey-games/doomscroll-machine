extends Card

@export var user_amount : int

var _controversy_hashtag : String
var _controversy_hashtag_symbol 

func _ready():
	_controversy_hashtag = User.hashtags.keys().pick_random()
	_controversy_hashtag_symbol = User.hashtags[_controversy_hashtag]
	super._ready()

func GetTitle():
	return _controversy_hashtag_symbol + " Controversy"

func GetShortDescription():
	return _controversy_hashtag_symbol + " 😠😠"

func GetLongDescription():
	return ("Spawns users with " 
		+ _controversy_hashtag_symbol  
		+ " dislike. Chance of removing " 
		+ _controversy_hashtag_symbol 
		+ " like of users.")

func onDraw():
	_controversy_hashtag = User.hashtags.keys().pick_random()
	_controversy_hashtag_symbol = User.hashtags[_controversy_hashtag]
	
	$CardTitle.text = GetTitle()
	UseShortDescription()

func PlayCard():
	print("Playing " + GetTitle())
	super.PlayCard()
	CE_spawnUsersWithHate.emit( _controversy_hashtag, user_amount )
	CE_userRemoveLikeWithChance.emit( _controversy_hashtag, 1.1)
