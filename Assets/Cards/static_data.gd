extends Node

var cardData = {}
var data_file_path = "res://Assets/Cards/DSM_database - Cards.json"

func _ready():
	cardData =	load_json_file(data_file_path)
	#print(cardData)

func load_json_file(filePath : String):
	if FileAccess.file_exists(filePath):
		var dataFile = FileAccess.open(filePath, FileAccess.READ)
		var parsedResult = JSON.parse_string(dataFile.get_as_text())
		if parsedResult is Dictionary:
			return parsedResult
		else:
			print("ERROR reading file")
	else:
		print("ERROR file doesnt exist")
