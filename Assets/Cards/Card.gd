extends Node2D
class_name Card 

signal CE_exposeHashtag( hashtag )

#region single user card effects signals
# Changes for each user the engagement by any of the factors in the array
# For example:
# Divisive card (50% chance of increasing or decreasing engagement for each user)
# the signal would send the array with -20%, 20% to indicate the engagement must be
# decreased / increased by 20%
signal CE_userPickRandomValueAndChange( factors_array )
 
# Removes Like hashtag from a User with chance.
# Like must be a string representing a hashtag
# Chance must be a float between 0.0 and 1.0 
signal CE_userRemoveLikeWithChance( like, chance )

# Removes Hate hashtag from a User with chance
# Like must be a string representing a hashtag
# Chance must be a float between 0.0 and 1.0 
signal CE_userRemoveHateWithChance( hate, chance )

# Removes hashtag from a User with change
# Like must be a string representing a hashtag
# Chance must be a float between 0.0 and 1.0 
signal CE_userRemoveHashtagWithChance( hashtag, chance )

# Prepares a boosts in users by [factor] to the next cards 
signal CE_userPrepareBoostFactor( factor )

# Changes engagement by [amount]
signal CE_userChangeEngagement(amount)

# Scrambles user like/dislikes
signal CE_userScrambleHashtags()

#endregion 

#region user-manager card effects signals  

# Spawns [amount] users with [hate] Hate
signal CE_spawnUsersWithHate( hate, amount )

# Spawns [amount] users with [like] Like
signal CE_spawnUsersWithLike( like, amount )

# Spawns [factor]*#users users. Factor must be a float greater than zero
signal CE_spawnUsersFactor(factor) 

# Spawns [amount] users
signal CE_spawnUsersAmount(amount)

# Kick most engaged [amount] users
signal CE_kickMostEngagedUser(amount : int)

# Kick [factor]*#users most engaged users. Factor must be a float in (0.0,1.0) 
signal CE_kickMostEngagedUsers(factor)

# Kick users with [like] Like and [hate] Hate with [chance] (between (0.0,1.0].
# Hate or Like can be null
signal CE_kickUsersWithDislike(like, hate, chance)

# Kick users with [chance] chance
signal CE_kickUsersWithChance(chance)

#endregion 

#region money card effects signals 

# Changes money by float [factor]
signal CE_changeMoneyByFactor(factor)

# Changes money by integer [amount] 
signal CE_changeMoneyByAmount(amount)

#endregion

var hashtag1: String = "POOP"
var hashtag2: String
var hashtag3: String

var Title : String
var ShortDescription: String
var LongDescription: String

var script_path  = "res://Assets/Cards/"
var BG_image_path = "res://Assets/CardArt/"
var BG_image_filename : String = "default.png"
var BackgroundImage : Texture

var id : String = "0"
var card_type = "Basic"

func _ready():
	pass

func LoadData(id_int):
	id = str(id_int)
	Title = read_static_data("title")
	hashtag1 = read_static_data("hashtag1")
	hashtag2 = read_static_data("hashtag2")
	hashtag3 = read_static_data("hashtag3")
	ShortDescription = read_static_data("short_description")
	LongDescription = read_static_data("long_description")
	BG_image_filename = read_static_data("background_image")
	card_type = read_static_data("type")
	
	BackgroundImage = load(get_bg_image_filepath())
	$CardTitle.text = Title
	$CardShortDescription.text = ShortDescription
	$CardLongDescription.text = LongDescription
	$CardBg.texture = BackgroundImage
	#test_print_card_data()

func Clone():
	var card_prefab: Node2D = self.duplicate()
	card_prefab.SetSpecialScript(id)
	card_prefab.LoadData(id)
	
	return card_prefab

func test_print_card_data():
	print("-------------------------------")
	print("> id=",id)
	print("> Title=",Title)
	print("> ShortDescription=",ShortDescription)
	print("> LongDescription=",LongDescription)
	print("> hashtags=[",hashtag1,"], [",hashtag2,"], [",hashtag3,"]")
	print("> image filepath=",get_bg_image_filepath())
	print("> card_type=",card_type)

func SetSpecialScript(id_int):
	id = str(id_int)
	card_type = read_static_data("type")
	if(card_type == "Special"):
		var script_filename = read_static_data("script")
		var script_filepath = script_path + script_filename
		self.set_script(load(script_filepath))

func _process(delta):
	pass

func read_static_data(column : String):
	var value = StaticData.cardData[id][column]
	if value != null:
		return value
	else:
		return ""

func GetHashtag1():
	return hashtag1 

func GetHashtag2(): 
	return hashtag2
	
func GetHashtag3():
	return hashtag3

func GetTitle():
	return Title

func GetShortDescription():
	return ShortDescription
	
func GetLongDescription():
	return LongDescription

func get_bg_image_filepath():
	return BG_image_path + BG_image_filename

func PlayCard():
	print("Playing card ", Title)
	var game_manager = get_tree().get_nodes_in_group("game_manager")[0]
	
	if game_manager is GameManager:
		game_manager.connect_to_card_effects(self)
	
	if hashtag1 != null && !hashtag1.is_empty():
		CE_exposeHashtag.emit(hashtag1)
	if hashtag2 != null && !hashtag2.is_empty():
		CE_exposeHashtag.emit(hashtag2)
	if hashtag3 != null && !hashtag3.is_empty():
		CE_exposeHashtag.emit(hashtag3)

func onDraw():
	pass

func UseLongDescription():
	$CardLongDescription.text = GetShortDescription() + "\n" + GetLongDescription()
	$CardShortDescription.visible = false
	$CardLongDescription.visible = true

func UseShortDescription():
	$CardShortDescription.text = GetShortDescription()
	$CardLongDescription.visible = false
	$CardShortDescription.visible = true
