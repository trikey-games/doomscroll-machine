extends Node2D
class_name GameManager

var bgm_pitch_mode = false
# Called when the node enters the scene tree for the first time.
func _ready():
	#var card = $Card
	add_to_group("game_manager")

func connect_to_card_effects(card : Card):
	print("Applying card effects")
	card.CE_exposeHashtag.connect(_on_expose_hashtag)
	
	card.CE_userPickRandomValueAndChange.connect(_on_CE_userPickRandomValueAndChange)
	card.CE_userRemoveLikeWithChance.connect(_on_CE_userRemoveLikeWithChance)
	card.CE_userRemoveHateWithChance.connect(_on_CE_userRemoveHateWithChance)
	card.CE_userRemoveHashtagWithChance.connect(_on_CE_userRemoveHashtagWithChance)
	card.CE_userPrepareBoostFactor.connect(_on_CE_userPrepareBoostFactor)
	card.CE_userChangeEngagement.connect(_on_CE_userChangeEngagement)
	card.CE_userScrambleHashtags.connect(_on_CE_userScrambleHashtags)
	
	card.CE_spawnUsersWithHate.connect(_on_CE_spawnUsersWithHate)
	card.CE_spawnUsersWithLike.connect(_on_CE_spawnUsersWithLike)
	card.CE_spawnUsersFactor.connect(_on_CE_spawnUsersFactor)
	card.CE_spawnUsersAmount.connect(_on_CE_spawnUsersAmount)
	
	card.CE_kickMostEngagedUser.connect(_on_CE_kickMostEngagedUser)
	card.CE_kickMostEngagedUsers.connect(_on_CE_kickMostEngagedUsers)
	card.CE_kickUsersWithDislike.connect(_on_CE_kickUsersWithDislike)
	
	card.CE_changeMoneyByFactor.connect(_on_CE_changeMoneyByFactor)
	card.CE_changeMoneyByAmount.connect(_on_CE_changeMoneyByAmount)


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func switch_music():
	if bgm_pitch_mode:
		$BGM_Funny.volume_db = -15
		$BGM_Funny_Agudo.volume_db = -80
	else:
		$BGM_Funny.volume_db = -80
		$BGM_Funny_Agudo.volume_db = -15
	bgm_pitch_mode = !bgm_pitch_mode


#region card effects for users
	
func _on_expose_hashtag(hashtag):
	for user in $UserSpawner.get_children():
		if user is User:
			user._on_CE_exposeHashtag(hashtag)

func _on_CE_userPickRandomValueAndChange( factors_array ):
	for user in $UserSpawner.get_children():
		if user is User:
			user._on_CE_userPickRandomValueAndChange(factors_array)

func _on_CE_userRemoveLikeWithChance( like, chance ):
	for user in $UserSpawner.get_children():
		if user is User:
			user._on_CE_userRemoveLikeWithChance(like, chance)

func _on_CE_userRemoveHateWithChance( hate, chance ):
	for user in $UserSpawner.get_children():
		if user is User:
			user._on_CE_userRemoveHateWithChance(hate, chance)

func _on_CE_userRemoveHashtagWithChance( hashtag, chance ):
	for user in $UserSpawner.get_children():
		if user is User:
			user._on_CE_userRemoveHashtagWithChance(hashtag, chance)

func _on_CE_userPrepareBoostFactor(factor):
	for user in $UserSpawner.get_children():
		if user is User:
			user._on_CE_userPrepareBoostFactor(factor)	

func _on_CE_userChangeEngagement(amount):
	for user in $UserSpawner.get_children():
		if user is User:
			user._on_CE_userChangeEngagement(amount)
			
func _on_CE_userScrambleHashtags():
	for user in $UserSpawner.get_children():
		if user is User:
			user._on_CE_userScrambleHashtags()

#endregion 

func _on_CE_spawnUsersWithHate( hate, amount ):
	$UserSpawner._on_CE_spawnUsersWithHate(hate, amount)

func _on_CE_spawnUsersWithLike( like, amount ):
	pass

func _on_CE_spawnUsersFactor(factor) :
	pass

func _on_CE_spawnUsersAmount(amount):
	$UserSpawner._on_CE_spawnUsersAmount(amount)

func _on_CE_kickMostEngagedUser(amount):
	$UserSpawner._on_CE_kickMostEngagedUser(amount)

func _on_CE_kickMostEngagedUsers(factor):
	$UserSpawner._on_CE_kickMostEngagedUserFactor(factor)

func _on_CE_kickUsersWithDislike(like, hate, chance):
	$UserSpawner._on_CE_kickUsersWithDislike(like,hate,chance)

func _on_CE_kickUsersWithChance(chance):
	$UserSpawner._on_CE_kickUsersWithChance(chance)

func _on_CE_changeMoneyByFactor(factor):
	($Scoring as Scoring).SetMultiplyFactor(factor)

func _on_CE_changeMoneyByAmount(amount):
	($Scoring as Scoring).AddMoney(amount)
#endregion
func _on_scoring_gm_end(totalMoney: int):
	StartMenu.starting_view = StartMenu.StartPanel.GAMEOVER
	HighScoreManager.score = totalMoney
	get_tree().change_scene_to_file("res://Scenes/Game/StartMenu/StartMenu.tscn")

func _on_deck_manager_gm_end():
	StartMenu.starting_view = StartMenu.StartPanel.MAINMENU
	HighScoreManager.score = 0
	get_tree().change_scene_to_file("res://Scenes/Game/StartMenu/StartMenu.tscn")
