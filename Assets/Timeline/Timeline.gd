extends Node2D

var initialPosition: Vector2 = Vector2(-250,350)
var initialScale: Vector2 = Vector2(2,2)
var initialSpeed: float = 1000

var timelineScale: Vector2 = Vector2(1,1)
var timelineSpeed: float = 0

var memes: Array[Sprite2D] = []
var memesPositions: Array[Vector2] = [
	Vector2(0,592),
	Vector2(0,380),
	Vector2(0,168),
	Vector2(0,-44),
	Vector2(0,-256)
]
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	var i:int = 0
	for meme in memes:
		if i >= memesPositions.size():
			meme.visible = false
			continue
		var expectedPosition = memesPositions[i]
		if meme.position != expectedPosition:
			meme.position = meme.position.move_toward(expectedPosition,delta*initialSpeed)
		if meme.scale != timelineScale:
			meme.scale = meme.scale.move_toward(timelineScale,delta*initialSpeed)
		i+=1 



func _on_deck_manager_tl_add_image(image):
	var meme:Sprite2D = $Meme.duplicate(15) as Sprite2D
	add_child(meme)
	memes.push_front(meme)
	meme.texture = image
	meme.position = initialPosition
	meme.scale = initialScale
	meme.visible = true
