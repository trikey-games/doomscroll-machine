extends ProgressBar

var minValue:float = 22
var maxValue:float = 1022

var minRelativeValue:float = 0
var maxRelativeValue:float = 100
var relativeValue:float = 0
# Called when the node enters the scene tree for the first time.
func _ready():
	min_value = minValue
	max_value = maxValue
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func _on_scoring_dl_set_max_value(value):
	maxRelativeValue = value

func _on_scoring_dl_reset_value():
	relativeValue = 0
	value = minValue
	
func _on_scoring_dl_change_value(value):
	var interval:float = maxValue-minValue
	var relativeInterval:float = maxRelativeValue-minRelativeValue
	relativeValue = value
	var newValue = (relativeValue*interval/relativeInterval) + minValue
	self.value = newValue
