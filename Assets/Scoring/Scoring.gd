extends Node2D
class_name Scoring

signal DL_setMaxValue(value: float)
signal DL_resetValue()
signal DL_ChangeValue(value: float)

signal MN_reset()
signal MN_setMoney(totalMoney:int)

signal GM_end(totalMoney:int)

var moneyPerUserPerInterval: int = 121
var moneyIntervalInSeconds: float = 1
var nextPayout: float = 0
var multiplyFactor: float = 1
var addedMoney: int = 0

var totalMoney: int = 0
var activeUsers: int = 0
var deadline: float = 60
var time: float = 0

func _ready():
	time = 0
	nextPayout = time+moneyIntervalInSeconds
	DL_setMaxValue.emit(deadline)
	DL_resetValue.emit()

func _process(delta):
	if multiplyFactor != 1:
		multiplyMoney(multiplyFactor)
		multiplyFactor = 1
	if addedMoney != 0:
		gainMoney(addedMoney)
		addedMoney = 0
	time += delta
	if time > nextPayout:
		var moneyGained: int = moneyPerUserPerInterval*activeUsers
		gainMoney(moneyGained)
		nextPayout += moneyIntervalInSeconds
	DL_ChangeValue.emit(time)
	if time >= deadline:
		GM_end.emit(totalMoney)

func gainMoney(moneyGained: int):
	totalMoney += moneyGained
	MN_setMoney.emit(totalMoney)
	
func multiplyMoney(factor: float):
	totalMoney = floori(factor * totalMoney)
	MN_setMoney.emit(totalMoney)

func SetMultiplyFactor(factor: float):
	multiplyFactor *= factor
	
func AddMoney(moneyAdded: int):
	addedMoney += moneyAdded

func _on_user_spawner_us_number_of_active_users_change(number:int):
	activeUsers = number
