extends RichTextLabel

var money: int = 0
var currentMoney: float = 0
var moneyIncrementSpeedPerSecond: float = 10000
var maxDigits: int = 10
var color = "cf9100"
var fadedColor = "cf910040"
# Called when the node enters the scene tree for the first time.
func _ready():
	money = 0
	currentMoney = 0


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if roundi(currentMoney) != money:
		var moneyDelta:float = delta*moneyIncrementSpeedPerSecond
		if currentMoney < money:
			currentMoney += moneyDelta
			if currentMoney > money:
				currentMoney = money
		else:
			currentMoney -= moneyDelta
			if currentMoney < money:
				currentMoney = money
	printMoney()

func printMoney():
	var moneyText:String = str(roundi(currentMoney))
	var digits = moneyText.length()
	var zeros = maxi(maxDigits - digits, 0)
	var labelText = "[center][code][color=" + color + "]$[color=" + fadedColor + "]"
	for i in range(zeros):
		labelText += "0"
	labelText += "[/color]" + moneyText + "[/color][/code][/center]"
	text = labelText

func _on_scoring_mn_reset():
	money = 0


func _on_scoring_mn_set_money(totalMoney):
	money = totalMoney
