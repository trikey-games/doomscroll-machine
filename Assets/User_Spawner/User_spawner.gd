extends Node2D
class_name UserSpawner

signal US_numberOfActiveUsersChange(number:int)

@export var interval : float = 2.0 : set = _interval_set
var rng = RandomNumberGenerator.new()

var numberActiveUsersLastFrame: int = 0

func _interval_set(value):
	interval = value
	$Timer.wait_time = interval

func _ready():
	pass
	#SpawnUser() #test

func _process(delta):
	var numberOfActiveUsers = getActiveUsers().size()
	if numberActiveUsersLastFrame != numberOfActiveUsers:
		US_numberOfActiveUsersChange.emit(numberOfActiveUsers)
		numberActiveUsersLastFrame = numberOfActiveUsers

func SpawnUser():
	for _i in self.get_children():
		if _i != $Timer and !_i.GetEnabled():
			_i.Spawn()
			break
		#print(_i)

var called = false
func _endgame():
	if !called:
		called = true
		for child in self.get_children():
			if child.has_method("_endgame"):
				child._endgame()
				
func _on_timer_timeout():
	SpawnUser() #test
	
func engagement_sorter(a : User,b : User):
	return a.engagement > b.engagement
		
func enabled_users(u : User):
	return u.enabled
	
func disabled_users(u : User):
	return !u.enabled
	
func getActiveUsers():
	var users = self.find_children("*","User",false,true)
	return users.filter(enabled_users)
	
func getInactiveUsers():
	var users = self.find_children("*","User",false,true)
	return users.filter(disabled_users)
	
func _on_CE_kickMostEngagedUser(amount):
	print("Kicking most engaged users ", amount)
	var users = getActiveUsers()
	users.sort_custom(engagement_sorter)
	for _i in range(amount):
		users[_i].ExpressBubble("😵")
		users[_i].Despawn()

func _on_CE_kickMostEngagedUserFactor(factor):
	print("Kicking most engaged users factor ", factor)
	var users = getActiveUsers()
	users.sort_custom(engagement_sorter)
	var bans = (users.size()*factor) as int
	for _i in range(bans):
		users[_i].Despawn()

func _on_CE_kickUsersWithDislike(like,hate,chance):
	print("Kicking Users with chance " , chance  
		, ((" like " + like) if like != null else "") 
		, ((" hate " + hate) if hate != null else "")   
		)
	var banusers = self.find_children("*","User",false,true)
	banusers = banusers.filter(func(u : User): return u.HasLike(like) or u.HasHate(hate))
	print("Possible users ", banusers.size())
	for u : User in banusers:
		if randf() < chance:
			print("Banning")
			u.Despawn()
			
func _on_CE_kickUsersWithChance(chance):
	var banusers = self.find_children("*","User",false,true)
	print("Possible users ", banusers.size())
	for u : User in banusers:
		if randf() < chance:
			print("Banning")
			u.Despawn()

func _on_CE_spawnUsersWithHate(hate, amount):
	print("Spawning users", amount, " with hate ", hate)
	var users = getInactiveUsers()
	for _i in range(amount):
		if _i >= users.size():
			break
		print("Spawning ", _i)
		users[_i].Spawn()
		users[_i].ForceHate(hate)
		users[_i].ExpressBubble("😠")
	

func _on_CE_spawnUsersAmount(amount):
	print("Spawning users ", amount)
	var users = getInactiveUsers()
	users.shuffle()
	for _i in range(amount):
		if _i >= users.size():
			break
		print("Spawning ", _i)
		users[_i].Spawn()
		users[_i].ExpressBubble("👀")
