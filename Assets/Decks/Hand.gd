extends Node2D
class_name Hand

var hand: Array[Card] = []
var positions: Array[float] = []

var highlighted: Card = null
var selected: Card = null

var maxHandPositionY: float = 175
var minHandPositionY: float = 525
var deckPosition: Vector2 = Vector2(0,850)
var handPositionX: float = 150
var minCardsInHand: int = 3
var selectedPosition: Vector2 = Vector2(400,350)

var normalScale: Vector2 = Vector2(0.5,0.5)
var highlightedScale: Vector2 = Vector2(0.65,0.65)
var selectedScale: Vector2 = Vector2(1,1)
var highlightThreshold: Vector2 = Vector2(75,100)

var cardSpeed: float = 1000.0

var playingCard:bool = false
# Called when the node enters the scene tree for the first time.
func _ready():
	highlighted = null
	playingCard = false
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if !playingCard:
		CheckHighlightedCard()
		HighlightCard()
		var cardsInHand = hand.size()
		if cardsInHand != positions.size():
			return
		for i in range(cardsInHand):
			var newY = positions[i]
			var newX = handPositionX
			if hand[i] == selected:
				newY = selectedPosition.y
				newX = selectedPosition.x
			if hand[i].position.y == newY && hand[i].position.x == newX:
				continue
			var newPosition = Vector2(newX,newY)
			hand[i].position = hand[i].position.move_toward(newPosition,delta*cardSpeed)
		
func CheckHighlightedCard():
	var mousePosition: Vector2 = get_local_mouse_position()
	for card in hand:
		var distance = (mousePosition - card.position).abs()
		if card != selected && distance.x < highlightThreshold.x && distance.y < highlightThreshold.y:
			if highlighted != card:
				UnHighlightCard()
				highlighted = card
			return
	UnHighlightCard()
	highlighted = null
	return

func HighlightCard():
	if highlighted != null:
		highlighted.set_scale(highlightedScale)
		highlighted.z_index = 1

func UnHighlightCard():
	if highlighted != null:
		highlighted.set_scale(normalScale)
		highlighted.z_index = 0
		highlighted = null

func SelectCard():
	if highlighted != null:
		if selected != null:
			UnSelectCard()
		if highlighted != null:
			selected = highlighted
			selected.z_index = 1
			UnHighlightCard()
			selected.UseLongDescription()
			selected.set_scale(selectedScale)
		

func UnSelectCard():
	if selected != null:
		selected.UseShortDescription()
		selected.set_scale(normalScale)
		selected.z_index = 0
		selected =null
		
func HasSelected():
	return selected != null

func PlaySelectedCard():
	if selected != null && !playingCard:
		playingCard = true
		var played = selected
		selected = null
		played.PlayCard()
		var index = played.get_index()
		hand.pop_at(index)
		RecalculatePositions()
		remove_child(played)
		played.queue_free()
		playingCard = false
		return index
	return -1

func AddCard(card:Card):
	#var handCard = card.duplicate(DUPLICATE_SCRIPTS) #as Card
	var handCard = card.Clone() #as Card
	handCard.position = deckPosition
	handCard.set_scale(normalScale)
	add_child(handCard)
	hand.push_back(handCard)
	RecalculatePositions()
	return
	
func RecalculatePositions():
	var cardsInHand = hand.size()
	positions.clear()
	var step: float = (maxHandPositionY - minHandPositionY) / (maxi(cardsInHand,minCardsInHand)-1 as float) 
	for i in range(cardsInHand):
		positions.append(maxHandPositionY - (i*step))
		
func EmptyHand():
	hand.clear()
	positions.clear()
	var children = self.get_children()
	for c in children:
		self.remove_child(c)
		c.queue_free()
