extends Node2D
class_name DeckManager

signal SB_enable()
signal SB_disable()

signal CD_enable()
signal CD_disable()
signal CD_setValue(value:float)

signal TL_addImage(image:Texture2D)

signal GM_end()

var cards: Array[Card]
var deck: Array = []
var hand: Array = []
var discard: Array = []
var card_prefab: Node2D = preload("res://Assets/Cards/Card.tscn").instantiate()

var handNode: Hand
var minCardsInHand: int = 3
var maxCardsInHand: int = 8

var selectingCard: bool = false
var playingCard: bool = false
var cooldown: float = 1.5
var coolingDown: bool = false
var timer: float = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	handNode = self.find_child("Hand",false,true) as Hand
	
	var player_deck = range(30) # genera 1 de cada carta
	#player_deck = [20,21,22,23,24,25,26,27,28,29,30] #test

	for id in player_deck:
		cards.append(new_card(id))
	
	deck = range(cards.size()) #indices
	deck.shuffle()
	for d in 3: draw()

func new_card(id : int):
	var new_card = card_prefab.duplicate()
	new_card.SetSpecialScript(id)
	new_card.LoadData(id)
	return new_card
	

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if handNode.HasSelected() && !playingCard:
		SB_enable.emit()
	else:
		SB_disable.emit()
	if hand.size() < minCardsInHand:
		draw()
	if coolingDown && playingCard:
		CD_enable.emit()
		if timer <= 0:
			coolingDown = false
			playingCard = false
		else:
			timer -= delta
			CD_setValue.emit(100.0 - (timer/cooldown) * 100.0)
	else:
		CD_disable.emit()
	pass
	
func _input(event):
	if event is InputEventKey and event.is_pressed():
		if event.get_keycode() == KEY_ESCAPE:
			GM_end.emit()
	if event is InputEventMouseButton and event.is_pressed():
		if event.get_button_index() == MOUSE_BUTTON_LEFT && !selectingCard:
			selectCard()
		if event.get_button_index() == MOUSE_BUTTON_RIGHT && !selectingCard:
			unselectCard()

func draw():
	if hand.size() == maxCardsInHand:
		print("there are no more space in hand")
		return
	
	if deck.size() == 0:
		if discard.size() != 0:
			deck.append_array(discard)
			discard.clear()
			deck.shuffle()
		else:
			print("there are no more cards to draw")
			return
	
	var index = deck.pop_front()
	hand.push_back(index)
	var card = cards[index]
	card.onDraw()
	handNode.AddCard(card)
	
func discardHand():
	discard.append_array(hand)
	hand.clear()
	handNode.EmptyHand()
	
func selectCard():
	selectingCard = true
	handNode.SelectCard()
	selectingCard = false

func unselectCard():
	selectingCard = true
	handNode.UnSelectCard()
	selectingCard = false

func playCard():
	playingCard = true
	var indexPlayed: int = handNode.PlaySelectedCard()
	if indexPlayed < 0:
		playingCard = false
		return
	var cardPlayed: int = hand.pop_at(indexPlayed)
	discard.push_back(cardPlayed)
	var timelineImage = cards[cardPlayed].BackgroundImage.duplicate(true)
	TL_addImage.emit(timelineImage)
	timer = cooldown
	coolingDown = true
	
	
func _on_button_pressed():
	$Sfx_Cork.play()
	playCard()
