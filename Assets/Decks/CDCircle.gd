extends TextureProgressBar


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass


func _on_deck_manager_cd_disable():
	visible = false


func _on_deck_manager_cd_enable():
	visible = true # Replace with function body.


func _on_deck_manager_cd_set_value(value):
	self.value = value
