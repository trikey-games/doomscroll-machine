extends Node2D
class_name User

@export var engagement_rate_base : float = 2.0
@export var engagement_rate : float = 2.0 : set = SetEngagementRate
@export var engagement : float = 50.0 : set = SetEngagement
@export var engagement_base : float = 50.0
var likes = []
var hates = []
var enabled = false
var hashtag_likes = 0
var hashtag_hates = 0
var hashtag_likes_max = 2
var hashtag_hates_max = 2
var rng = RandomNumberGenerator.new()
var boost : float = 1.0

#enum Hashtags {🐈,📺,💩,🎵,👽,🎮}
static var hashtags = {
	"CAT":"🐈",
	"TV":"📺",
	"POOP":"💩",
	"MUSIC":"🎵",
	"ALIEN":"👽",
	"GAME":"🎮"
	}

func SetEngagementRate(value):
	engagement_rate = value

func SetEngagement(value):
	engagement = value
	if (engagement > 100):
		engagement = 100
	
func set_enabled(value):
	enabled = value

func GetEnabled():
	return enabled

func get_hashtag_dict():
	pass

func _ready():
	$UserProfile.visible = false
	$UserProfile/UserBubble.visible = false

func AddEngagement(value):
	engagement += value

func drain_engagement(rate, delta):
	engagement = engagement - rate * delta

func ExpressBubble(message):
	if enabled:
		$UserProfile/UserBubble/BubbleMessage.text = message
		$UserProfile/UserBubble/BubbleTimer.wait_time = 2
		$UserProfile/UserBubble/BubbleTimer.start()
		$UserProfile/UserBubble.visible = true

func _on_bubble_timer_timeout():
	$UserProfile/UserBubble.visible = false
	
func Spawn():
	$UserProfile.visible = true
	set_enabled(true)
	reset_values()

func Despawn():
	$UserProfile.visible = false
	set_enabled(false)

func reset_values():
	engagement = engagement_base
	SetEngagementRate(engagement_rate_base)
	recreate_hashtags()

func recreate_hashtags():
	likes = []
	hates = []
	hashtag_likes = rng.randi_range(1, hashtag_likes_max)
	hashtag_hates = rng.randi_range(1, hashtag_hates_max)
	for l in hashtag_likes:
		var hashtag = GetRandomHashtag()
		while likes.has(hashtag):
			hashtag = GetRandomHashtag()
		likes.append(hashtag)
		
	for h in hashtag_hates:
		var hashtag = GetRandomHashtag()
		while hates.has(hashtag) or likes.has(hashtag):
			hashtag = GetRandomHashtag()
		hates.append(hashtag)
		
	#print(likes)
	#print(hates)
	print_emojis()

func print_emojis():
	var likes_string = ""
	for l in likes:
		likes_string = likes_string + "#" + hashtags[l] + "\n"
	var hates_string = ""
	for h in hates:
		hates_string = hates_string + "#" + hashtags[h] + "\n"
	$"UserProfile/Hashtags-like".text = likes_string
	$"UserProfile/Hashtags-hate".text = hates_string

func GetRandomHashtag():
	var size = hashtags.size()
	var random_key = hashtags.keys()[randi() % size]
	return random_key
	
func HasLike(like):
	return likes.has(like)

func HasHate(hate):
	return hates.has(hate)
	
func ForceHate(hate):
	likes.erase(hate)
	if hates.size() < 2:
		hates.append(hate)
	else:
		hates[1] = hate
	print_emojis()

func _process(delta):
	if(enabled):
		drain_engagement(engagement_rate, delta)
		$UserProfile/ProgressBar.value = engagement
		if engagement <= 0:
			Despawn()
			
			
#region card effects 
func _on_CE_exposeHashtag( hashtag ):
	#print("Exposed to ", hashtag)
	if enabled: 
		if( likes.has(hashtag) ):
			SetEngagement(engagement * 1.25 * boost)
			ExpressBubble("😂")
		if( hates.has(hashtag) ):
			SetEngagement(engagement * 0.6 * boost)
			ExpressBubble("😡")
	boost = 1.0


func _on_CE_userPickRandomValueAndChange( factors_array ):
	if enabled:
		var value = factors_array[ randi() % factors_array.size() ]
		if value > 0:
			ExpressBubble("😊")
		else:
			ExpressBubble("😠")
		SetEngagement(engagement + value)
		print("Engagement ", engagement, value)

func _on_CE_userRemoveLikeWithChance( like, chance ):
	if enabled: 
		if randf() < chance:
			ExpressBubble("❓")
			likes.erase(like)
		print_emojis()

func _on_CE_userRemoveHateWithChance( hate, chance ):
	if enabled: 
		if randf() < chance:
			ExpressBubble("❓")
			hates.erase(hate)
		print_emojis()

func _on_CE_userRemoveHashtagWithChance( hashtag, chance ):
	if enabled:
		if randf() < chance:
			ExpressBubble("❓")
			likes.erase(hashtag)
			hates.erase(hashtag)
		print_emojis()
	
func _on_CE_userPrepareBoostFactor(factor):
	print("Boosting next by ", factor)
	if enabled:
		ExpressBubble("👀")
		boost = factor

func _on_CE_userChangeEngagement(amount):
	print("Engagement change ", amount)
	if enabled:
		if amount > 0:
			ExpressBubble("😊")
		else:
			ExpressBubble("😠")
		SetEngagement(engagement + amount)
	
func _on_CE_userScrambleHashtags():
	print("Scramble")
	if enabled:
		ExpressBubble("😵‍")
		recreate_hashtags()

#endregion 



